/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface.stockpanels;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.PlotOrientation;
import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public abstract class JFreeChartLinePanel extends GraphPanel{
    
    public JFreeChartLinePanel(Symbol symbol, String title, String yaxis)
    {
        super(symbol);
        chart = ChartFactory.createLineChart(title, "Time", yaxis, dataset, PlotOrientation.VERTICAL, true, true, true);
        panel = new ChartPanel(chart);
        
        this.add(panel);
    }
    
    @Override
    public void updateDataset(double newData)
    {
        dataset.addValue(newData/100, getSymbol().getSymbolName(), (""+ getSymbol().getCurrentTimestamp()));
        if (getSymbol().getCurrentTimestamp() - getEarliestTick() > GraphPanel.NUM_TICKS_TO_TRACK)
        {
            dataset.removeValue(getSymbol().getSymbolName(), (""+getEarliestTick()));
            incrementEarliestTick();
        }
    }
}
