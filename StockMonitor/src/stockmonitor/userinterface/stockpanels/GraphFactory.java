/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface.stockpanels;

import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public class GraphFactory {
    public static GraphPanel getGraph(String type, Symbol symbol)
    {
        switch(type)
        {
            case "AskPrice":
                return new StockAskPriceGraph(symbol);
            case "BidPrice":
                return new StockBidPriceGraph(symbol);
            case "PriceChange":
                return new StockPriceChangeGraph(symbol);
            case "Price":
                return new StockPriceGraph(symbol);
            case "Volume":
                return new StockVolumeGraph(symbol);
            default:
                return null;
        }
    }
}
