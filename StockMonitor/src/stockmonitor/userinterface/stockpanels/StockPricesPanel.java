/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface.stockpanels;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import stockmonitor.core.Observer;
import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public class StockPricesPanel extends JPanel implements TableModelListener{
    private final StockPriceTableModel model;
    
    public StockPricesPanel(List<Symbol> symbols)
    {
        model = new StockPriceTableModel(symbols);
        model.addTableModelListener(this);
        JTable table = new JTable();
        table.setModel(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.CENTER);
        setVisible(true);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        revalidate();
        repaint();
    }
    
    public final void startListening()
    {
        model.startListening();
    }
    
    public final void stopListening()
    {
        model.stopListening();
    }
    
    class StockPriceTableModel extends AbstractTableModel implements Observer {
        private final List<Symbol> _symbols;
        private final String[] columnNames;
        
        public StockPriceTableModel(List<Symbol> symbols)
        {
            _symbols = new ArrayList<>();
            columnNames = new String[]{"Stock", "Opening", "Current", "Direction", "Bid", "Ask"};
            
            for (Symbol symbol: symbols)
            {
                _symbols.add(symbol);
            }
            startListening();
        }
        
        @Override
        public String getColumnName(int col)
        {
            return columnNames[col];
        }
        
        @Override
        public int getRowCount() {
            return _symbols.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Symbol symbol = _symbols.get(rowIndex);
            switch (columnIndex)
            {
                case 0:
                    return symbol.getSymbolName();
                case 1:
                    return symbol.getOpeningPrice()/100;
                case 2:
                    return symbol.getCurrentPrice()/100;
                case 3:
                    // Needs changed later to return one of two images
                    return symbol.getCurrentPrice()/100 - symbol.getLastPrice()/100;
                case 4:
                    return symbol.getBidPrice()/100;
                case 5:
                    return symbol.getAskPrice()/100;
                default:
                    return null;
            }
        }
        
        public final void startListening()
        {
            for (Symbol sym: _symbols)
            {
                sym.register(this);
            }
        }

        public final void stopListening()
        {
            for (Symbol sym: _symbols)
            {
                sym.unregister(this);
            }
        }
        
        @Override
        public void update() {
            this.fireTableDataChanged();
        }
    }
}
