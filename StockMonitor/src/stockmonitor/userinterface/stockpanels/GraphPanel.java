/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface.stockpanels;

import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import stockmonitor.core.Observer;
import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public abstract class GraphPanel extends JPanel implements Observer{
    private final Symbol _symbol;
    public static final int NUM_TICKS_TO_TRACK = 60*60;
    private long earliestTick;
    protected final  DefaultCategoryDataset dataset;
    protected JFreeChart chart;
    protected ChartPanel panel;
    
    public GraphPanel(Symbol symbol)
    {
        _symbol = symbol;
        _symbol.register(this);
        dataset = new DefaultCategoryDataset();
        
        earliestTick = _symbol.getCurrentTimestamp();
        
        startListening();
        
        setVisible(true);
    }
    
    public long getEarliestTick()
    {
        return earliestTick;
    }
    
    protected void incrementEarliestTick()
    {
        earliestTick++;
    }
    
    public Symbol getSymbol()
    {
        return _symbol;
    }
    
    public final void stopListening()
    {
        _symbol.unregister(this);
    }
    
    public final void startListening()
    {
        _symbol.register(this);
    }
    
    public abstract void updateDataset(double newData);
}
