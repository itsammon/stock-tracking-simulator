/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface.stockpanels;

import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public class StockAskPriceGraph extends JFreeChartLinePanel{
    
    public StockAskPriceGraph(Symbol symbol)
    {
        super(symbol, symbol.getSymbolName(), "Ask Price");
    }

    @Override
    public void update() {
        updateDataset(getSymbol().getAskPrice());
    }
    
}
