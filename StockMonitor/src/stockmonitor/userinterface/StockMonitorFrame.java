/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import stockmonitor.core.CSVLoader;
import stockmonitor.core.Communicator;
import stockmonitor.core.Portfolio;
import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public class StockMonitorFrame extends JFrame implements ActionListener{
    
    static class Shutdown extends Thread
    {
        @Override
        public void run()
        {
            comm.stop();
        }
    }
    private JMenuBar menuBar;
    private Portfolio portfolio;
    private static Communicator comm;
    private static CSVLoader csv;
    private PortfolioTextArea portText;
    
    public StockMonitorFrame(String name)
    {
        super(name);
        csv = new CSVLoader("CompanyList.csv");
        portfolio = new Portfolio(csv);
        comm = new Communicator();
         
        // Add a shutdown hook to close network comunications when the program exits
        addShutdownBehavoir();
       
        try {
            comm.setRemoteEndPoint("ec2-54-202-195-90.us-west-2.compute.amazonaws.com");
            comm.setPortfolio(portfolio);
            //comm.setRemoteEndPoint("ec2-107-23-252-140.compute-1.amazonaws.com");
        
            // Set minimum size and behaviors
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setMinimumSize(new Dimension(400,400));

            // Set up the content pane
            setLayout(new FlowLayout());

            // Add the menu bars
            createMenuBar();

            // For testing add in a JTextArea that we will write to
            addTextArea();

            // Set the loacation of the frame
            pack();
            setLocationRelativeTo(null);

            // Start the network observer
            comm.start();
        } catch (UnknownHostException ex) {
            Logger.getLogger(StockMonitorFrame.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Unable to connect to server");
        }
    }
    
    private void addTextArea()
    {
        portText = new PortfolioTextArea(portfolio);
        this.getContentPane().add(portText);
    }
    
    private void createMenuBar()
    {
        JMenu fileMenu;
        JMenu portMenu;
        JMenu panelMenu;
        JMenuItem menuItem;
        menuBar = new JMenuBar();
        
        // Create the main file menu
        
        fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        
        menuItem = new JMenuItem("New Portfolio");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
        
        menuItem = new JMenuItem("Load Portfolio");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
        
        menuItem = new JMenuItem("Save Portfolio");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
        
        menuItem = new JMenuItem("Quit");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
        
        // Create the portfolio menu
        portMenu = new JMenu("Portfolio");
        menuItem.addActionListener(this);
        menuBar.add(portMenu);
        
        menuItem = new JMenuItem("Add Stock");
        menuItem.addActionListener(this);
        portMenu.add(menuItem);
        
        menuItem = new JMenuItem("Remove Stock");
        menuItem.addActionListener(this);
        portMenu.add(menuItem);
        
        // Create the panel menu
        panelMenu = new JMenu("View");
        menuBar.add(panelMenu);
        
        menuItem = new JMenuItem("New Stock Prices Panel");
        menuItem.addActionListener(this);
        panelMenu.add(menuItem);
        
        menuItem = new JMenuItem("New Stock Price Graph");
        menuItem.addActionListener(this);
        panelMenu.add(menuItem);
        
        menuItem = new JMenuItem("New Stock Ask Price Graph");
        menuItem.addActionListener(this);
        panelMenu.add(menuItem);
        
        menuItem = new JMenuItem("New Stock Bid Price Graph");
        menuItem.addActionListener(this);
        panelMenu.add(menuItem);
        
        menuItem = new JMenuItem("New Stock Price Change Graph");
        menuItem.addActionListener(this);
        panelMenu.add(menuItem);
        
        menuItem = new JMenuItem("New Stock Volume Graph");
        menuItem.addActionListener(this);
        panelMenu.add(menuItem);
        
        setJMenuBar(menuBar);
    }
    
    private void newPortfolio()
    {
        portfolio = new Portfolio(csv);
        comm.setPortfolio(portfolio);
        
        // Edit the text area to display the portfolio
        portText.setPortfolio(portfolio);
        
        repaint();
        this.setVisible(true);
    }
    
    private void loadPortfolio()
    {
        // Get file from the chooser
        JFileChooser chooser = new JFileChooser();
        int choice = chooser.showOpenDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION)
        {
            try {
                // Load the portfolio
                portfolio = new Portfolio(csv, chooser.getSelectedFile());
                comm.setPortfolio(portfolio);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Load Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        repaint();
        this.setVisible(true);
    }
    
    private void savePortfolio()
    {
        // Get the file from the chooser
        JFileChooser chooser = new JFileChooser();
        int choice = chooser.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION)
        {
            try {
                // Save the portfolio
                portfolio.savePortfolio(chooser.getSelectedFile());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Save Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void addStock()
    {
        List<String> options = new ArrayList<>();
        
        for (String name : csv.getSymbols())
        {
            if (portfolio.getSymbol(name) == null)
            {
                options.add(name);
            }
        }
        List<String> stocks = ListDialog.showDialog(this, null, options, true);
        
        for (String stock : stocks)
        {
            try {
                portfolio.addSymbol(stock);
            } catch (Exception ex) {
                Logger.getLogger(StockMonitorFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        repaint();
    }

    private void removeStock()
    {
        List<String> stocks = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), true);
        
        for (String stock : stocks)
        {
            portfolio.removeSymbol(stock);
        }
        repaint();
    }
    
    private void newStockPriceGraph()
    {
        List<String> symbolNames = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), false);
        for (String name : symbolNames)
        {
            JFrame newGraph = GraphFrameFactory.getFrame(portfolio.getSymbol(name), "Price", true, true);
        }
    }

    private void newStockVolumeGraph()
    {
        List<String> symbolNames = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), false);
        for (String name : symbolNames)
        {
            JFrame newGraph = GraphFrameFactory.getFrame(portfolio.getSymbol(name), "Volume", false, false);
        }
    }
    
    private void newStockAskPriceGraph()
    {
        List<String> symbolNames = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), false);
        for (String name : symbolNames)
        {
            JFrame newGraph = GraphFrameFactory.getFrame(portfolio.getSymbol(name), "AskPrice", false, false);
        }
    }
    
    private void newStockBidPriceGraph()
    {
        List<String> symbolNames = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), false);
        for (String name : symbolNames)
        {
            JFrame newGraph = GraphFrameFactory.getFrame(portfolio.getSymbol(name), "BidPrice", false, false);
        }
    }
    
    private void newStockPriceChangeGraph()
    {
        List<String> symbolNames = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), false);
        for (String name : symbolNames)
        {
            JFrame newGraph = GraphFrameFactory.getFrame(portfolio.getSymbol(name), "PriceChange", false, false);
        }
    }
    
    private void newStockPricePanel()
    {
        List<String> symbolNames = ListDialog.showDialog(this, null, portfolio.getSymbolNames(), true);
        List<Symbol> symbols = new ArrayList<>();
        for (String name : symbolNames)
        {
            symbols.add(portfolio.getSymbol(name));
        }
        JFrame newFrame = new StockPricesFrame(symbols);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        
        switch (source.getText()) {
            case "Quit":
                System.exit(0);
            case "New Portfolio":
                newPortfolio();
                break;
            case "Load Portfolio":
                loadPortfolio();
                break;
            case "Save Portfolio":
                savePortfolio();
                break;
            case "Add Stock":
                addStock();
                break;
            case "Remove Stock":
                removeStock();
                break;
            case "New Stock Prices Panel":
                newStockPricePanel();
                break;
            case "New Stock Price Graph":
                newStockPriceGraph();
                break;
            case "New Stock Volume Graph":
                newStockVolumeGraph();
                break;
            case "New Stock Ask Price Graph":
                newStockAskPriceGraph();
                break;
            case "New Stock Bid Price Graph":
                newStockBidPriceGraph();
                break;
            case "New Stock Price Change Graph":
                newStockPriceChangeGraph();
                break;
            default:
                break;
        }
    }
    
    private void addShutdownBehavoir()
    {
        Runtime.getRuntime().addShutdownHook(new Shutdown());
    }
}
