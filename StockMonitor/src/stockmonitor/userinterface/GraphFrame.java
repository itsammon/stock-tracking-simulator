/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import javax.swing.JPanel;
import stockmonitor.core.Symbol;
import stockmonitor.userinterface.stockpanels.GraphFactory;
import stockmonitor.userinterface.stockpanels.GraphPanel;

/**
 *
 * @author ammon
 */
public class GraphFrame extends AbstractGraphFrame{
    private final GraphPanel graph;
    
    public GraphFrame(String title, Symbol symbol, String type)
    {
        super(title, symbol);
        graph = GraphFactory.getGraph(type, symbol);
    }
    

    @Override
    public GraphPanel addGraph() {
        return graph;
    }
    
    @Override
    public void update() {
        // Don't need to do anything on update since graph takes care of it.
        // This function is required since we are an observer
    }

    @Override
    public JPanel topPanel() {
        // Give a blank panel to work with
        return new JPanel();
    }
}
