/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import stockmonitor.core.Symbol;

/**
 *
 * @author ammon
 */
public class GraphFrameFactory {
    public static AbstractGraphFrame getFrame(Symbol symbol, String type, boolean closePrice, boolean openPrice)
    {
        AbstractGraphFrame frame;
        switch(type)
        {
            case "AskPrice":
                frame = new GraphFrame("Ask Price: " + symbol.getSymbolName(), symbol, type);
                break;
            case "BidPrice":
                frame = new GraphFrame("Bid Price: " + symbol.getSymbolName(), symbol, type);
                break;
            case "PriceChange":
                frame = new GraphFrame("Stock Price Changes: " + symbol.getSymbolName(), symbol, type);
                break;
            case "Price":
                frame = new GraphFrame("Stock Price: " + symbol.getSymbolName(), symbol, type);
                break;
            case "Volume":
                frame = new GraphFrame("Stock Volume: " + symbol.getSymbolName(), symbol, type);
                break;
            default:
                // Should do something better here if the type is invalid
                return null;
        }
        
        // Add on close price or openPrice if desired
        if (closePrice)
        {
            frame = new GraphWithClosePrice(frame.getTitle(), frame);
        }
        if (openPrice)
        {
            frame = new GraphWithOpenPrice(frame.getTitle(), frame);
        }
        frame.initialize();
        return frame;
    }
}
