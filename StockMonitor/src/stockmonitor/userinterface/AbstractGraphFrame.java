/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import stockmonitor.core.Observer;
import stockmonitor.core.Symbol;
import stockmonitor.userinterface.stockpanels.GraphPanel;

/**
 *
 * @author ammon
 */
public abstract class AbstractGraphFrame extends JFrame implements Observer{
    private final Symbol symbol;
    
    public AbstractGraphFrame(String title, Symbol symbol)
    {
        super(title);
        this.symbol = symbol;
        
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
    }
    
    public final void initialize()
    {
        add(topPanel(), BorderLayout.PAGE_START);
        add(addGraph(), BorderLayout.CENTER);
        
        startListening();
        pack();
        setVisible(true);
    }
    
    public abstract JPanel topPanel();
    
    public abstract GraphPanel addGraph();
    
    public final Symbol getSymbol()
    {
        return symbol;
    }
    
    public final void startListening()
    {
        symbol.register(this);
    }
    
    public final void stopListening()
    {
        symbol.unregister(this);
    }
}
