/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.Label;
import javax.swing.JPanel;
import stockmonitor.userinterface.stockpanels.GraphPanel;

/**
 *
 * @author ammon
 */
public class GraphWithOpenPrice extends GraphFrameDecorator {
    private Label openPriceLabel;
    private double prevOpenPrice;
    
    public GraphWithOpenPrice(String title, AbstractGraphFrame component)
    {
        super(title, component);
        prevOpenPrice = getSymbol().getOpeningPrice();
    }

    @Override
    public GraphPanel addGraph() {
        return component.addGraph();
    }

    @Override
    public void update() {
        if (prevOpenPrice != getSymbol().getOpeningPrice())
        {
            prevOpenPrice = getSymbol().getOpeningPrice();
            openPriceLabel.setText("Open: " + prevOpenPrice/100);
        }
        invalidate();
        repaint();
    }

    @Override
    public JPanel topPanel() {
        JPanel topPanel = component.topPanel();
        openPriceLabel = new Label("Open: " + prevOpenPrice/100);
        topPanel.add(openPriceLabel);
        return topPanel;
    }
}
