/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

/**
 *
 * @author ammon
 */
public abstract class GraphFrameDecorator extends AbstractGraphFrame {
    protected AbstractGraphFrame component;
    
    public GraphFrameDecorator(String title, AbstractGraphFrame component)
    {
        super(title, component.getSymbol());
        this.component = component;
    }
    
}
