/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.BorderLayout;
import java.util.List;
import javax.swing.JFrame;
import stockmonitor.core.Symbol;
import stockmonitor.userinterface.stockpanels.StockPricesPanel;

/**
 *
 * @author ammon
 */
public class StockPricesFrame extends JFrame{
    
    public StockPricesFrame(List<Symbol> symbols)
    {
        super("Stock Prices");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
        
        this.add(new StockPricesPanel(symbols), BorderLayout.CENTER);
        
        pack();
        setVisible(true);
    }
}
