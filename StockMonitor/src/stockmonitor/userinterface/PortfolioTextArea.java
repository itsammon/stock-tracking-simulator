/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import stockmonitor.core.Observer;
import stockmonitor.core.Portfolio;

/**
 *
 * @author ammon
 */
public class PortfolioTextArea extends JPanel implements Observer{
    private Portfolio port;
    private JTextArea textArea;
    private JScrollPane scrollPane;
    
    public PortfolioTextArea(Portfolio portfolio)
    {
        port = portfolio;
        port.register(this);
        textArea = new JTextArea(20,15);
        textArea.setEditable(false);
        setText();
        scrollPane = new JScrollPane(textArea);
        
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(250,145));
        scrollPane.setMinimumSize(new Dimension(10,10));
        
        add(scrollPane);
        setVisible(true);
    }
    
    public void setPortfolio(Portfolio portfolio)
    {
        port.unregister(this);
        port = portfolio;
        port.register(this);
        setText();
    }
    
    private void setText()
    {
        textArea.setText("Portfolio Symbols: \n");
        
        for (String symbol: port.getSymbolNames())
        {
            textArea.append(symbol+"\n");
        }
    }

    @Override
    public void update() {
        setText();
    }
}
