/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author ammon
 */
public class ListDialog extends JDialog implements ActionListener {
    private static ListDialog dialog;
    private static List<String> _options;
    private static List<String> values = new ArrayList<>();
    private JList list;
    
    public static List<String> showDialog(Component frameComp,
                    Component locationComp,
                    List<String> options, boolean multiSelect)
    {
        _options = options;
        values = new ArrayList<>();
        Frame frame = JOptionPane.getFrameForComponent(frameComp);
        dialog = new ListDialog(frame, locationComp, multiSelect);
        dialog.setVisible(true);
        return values;
    }
    
    private static void addValue(String newValue)
    {
        if (!values.contains(newValue))
        {
            values.add(newValue);
        }
    }
    
    private ListDialog(Frame frame, Component locationComp, boolean multiSelect)
    {
        super(frame, "Select Stocks", true);
        
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        
        final JButton finishButton = new JButton("Finish");
        finishButton.setActionCommand("Finish");
        finishButton.addActionListener(this);
        
        // Set the default button to be the finish button
        getRootPane().setDefaultButton(finishButton);
        Collections.sort(_options);
        list = new JList(_options.toArray());                
                
        if (multiSelect == true)
        {
            list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        }
        else
        {
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }
        
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(25);
        
        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(250,250));
        
        // Create container for the title
        JPanel listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
        listPane.add(listScroller);
        listPane.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
        buttonPane.setBorder(BorderFactory.createEmptyBorder(0,10,10,10));
        buttonPane.add(cancelButton);
        buttonPane.add(finishButton);
        
        Container contentPane = getContentPane();
        contentPane.add(listPane, BorderLayout.CENTER);
        contentPane.add(buttonPane, BorderLayout.PAGE_END);
        
        pack();
        setLocationRelativeTo(locationComp);
    }
        
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if ("Finish".equals(e.getActionCommand()))
        {
            for (Object name : list.getSelectedValuesList())
            {
                ListDialog.addValue((String) name);
            }
        }
        ListDialog.dialog.setVisible(false);
    }
}
