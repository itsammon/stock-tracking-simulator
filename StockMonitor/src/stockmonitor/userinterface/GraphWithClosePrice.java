/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.userinterface;

import java.awt.Label;
import javax.swing.JPanel;
import stockmonitor.userinterface.stockpanels.GraphPanel;

/**
 *
 * @author ammon
 */
public class GraphWithClosePrice extends GraphFrameDecorator{
    private Label prevClosePriceLabel;
    private double prevPrevClosePrice;
    
    public GraphWithClosePrice(String title, AbstractGraphFrame component)
    {
        super(title, component);
        prevPrevClosePrice = getSymbol().getPrevClosingPrice();
    }

    @Override
    public GraphPanel addGraph() {
        return component.addGraph();
    }

    @Override
    public void update() {
        if (prevPrevClosePrice != getSymbol().getPrevClosingPrice())
        {
            prevClosePriceLabel.setText("Prev. close: " + getSymbol().getPrevClosingPrice()/100);
            prevPrevClosePrice = getSymbol().getPrevClosingPrice();
        }
        invalidate();
        repaint();
    }

    @Override
    public JPanel topPanel() {
        JPanel topPanel = component.topPanel();
        prevClosePriceLabel = new Label("Prev. close: " + getSymbol().getPrevClosingPrice()/100);
        topPanel.add(prevClosePriceLabel);
        return topPanel;
    }
    
}
