/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import Messages.TickerMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class Portfolio extends Subject{
    private CSVLoader csv;
    private ConcurrentMap<String, Symbol> symbols;
    
    public Portfolio(CSVLoader csv)
    {
        this.csv = csv;
        symbols = new ConcurrentHashMap<>();
    }
    
    public Portfolio(CSVLoader csv, File inFile) throws FileNotFoundException, Exception
    {
        this.csv = csv;
        loadPortfolio(inFile);
    }
    
    public void addSymbol(Symbol symbol) throws Exception
    {
       if (csv.isValid(symbol.getSymbolName()) && !symbols.containsKey(symbol.getSymbolName()))
       {
           symbols.put(symbol.getSymbolName(), symbol);
           notifyObservers();
       }
       else
       {
            throw new Exception("Unknown symbol");
       }
    }
    
    public void addSymbol(String symbol) throws Exception
    {
       System.out.println("Adding symbol");
       if (csv.isValid(symbol) && !symbols.containsKey(symbol))
       {
           symbols.put(symbol, new Symbol(symbol));
           notifyObservers();
       }
       else
       {
            throw new Exception("Unknown symbol");
       }
    }
    
    public void addSymbols(List<Symbol> symbols) throws Exception
    {
        for (Symbol symbol: symbols)
        {
           if (csv.isValid(symbol.getSymbolName()) && !this.symbols.containsKey(symbol.getSymbolName()))
           {
               this.symbols.put(symbol.getSymbolName(), symbol);
           }
           else
           {
               throw new Exception("Unknown symbol");
           }
        }
        notifyObservers();
    }
    
    public void removeSymbol(String symbol)
    {
        symbols.remove(symbol);
        notifyObservers();
    }
    
    public void removeSymbol(Symbol symbol)
    {
        symbols.remove(symbol.getSymbolName());
        notifyObservers();
    }
    
    public void removeSymbols(List<Symbol> symbols)
    {
        for (Symbol symbol: symbols)
        {
            this.symbols.remove(symbol.getSymbolName());
        }
        notifyObservers();
    }
    
    public void updateSymbol(TickerMessage message)
    {
        String symbol = message.getSymbol();
        symbols.get(symbol).update(message);
    }
    
    public List<String> getSymbolNames()
    {
        List<String> temp = new ArrayList<>();
        Set<String> names = this.symbols.keySet();
        for (String name : names)
        {
            temp.add(name);
        }
        return temp;
    }
    
    public List<Symbol> getSymbols()
    {
        List<Symbol> temp = new ArrayList<>();
        Collection<Symbol> syms = this.symbols.values();
        for (Symbol symbol: syms)
        {
            temp.add(symbol);
        }
        return temp;
    }
    
    public Symbol getSymbol(String name)
    {
        return symbols.get(name);
    }
    
    public void savePortfolio(File file) throws IOException
    {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(csv.getFilename() + "\n");
            
            for (String stock : symbols.keySet())
            {
                writer.write(stock + "\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(Portfolio.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    public final void loadPortfolio(File file) throws FileNotFoundException, Exception
    {
        BufferedReader reader = null;
        String line;
        String[] splitLine;
        
        // Try to load the csv file
        try {
            reader = new BufferedReader(new FileReader(file));
            Scanner scan = new Scanner(reader);
            
            // Create the symbol table
            symbols = new ConcurrentHashMap<>();
            
            // Load the csvFile name
            line = scan.nextLine();
            if (!line.equals(csv.getFilename()))
                throw new Exception("CSV File does not match");
            
            // Load all the symbols from the file
            while (scan.hasNextLine())
            {
                line = scan.nextLine();
                addSymbol(line);
            }
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Portfolio.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
