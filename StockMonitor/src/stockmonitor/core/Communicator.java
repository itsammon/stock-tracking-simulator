/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import Messages.StreamStocksMessage;
import Messages.TickerMessage;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class Communicator implements Observer, Runnable {
    
    private boolean _isMonitoring;
    private DatagramSocket socket;
    private InetAddress remoteEndPoint;
    private int port;
    private Portfolio portfolio;
    
    public void setRemoteEndPoint(String addr) throws UnknownHostException
    {
        remoteEndPoint = InetAddress.getByName(addr);
    }
    
    public InetAddress getRemoteEndPoint()
    {
        return remoteEndPoint;
    }
    
    public String status()
    {
        if (_isMonitoring)
        {
            return "Monitoring";
        }
        else
        {
            return "";
        }
    }
    
    public void setPortfolio(Portfolio port)
    {
        stop();
        if (portfolio != null)
        {
            portfolio.unregister(this);
        }
        
        portfolio = port;
        portfolio.register(this);
        start();
    }
    
    public Portfolio getPortfolio()
    {
        return portfolio;
    }
    
    public void start()
    {
        try {
            port = 12099;
            socket = new DatagramSocket();
            
            _isMonitoring = true;
            // Start monitoring thread
            Thread monitoring = new Thread(this);
            monitoring.start();
        } catch (SocketException ex) {
            Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void stop()
    {
        _isMonitoring = false;
        
        // Close socket
        if (socket != null)
        {
            send(new StreamStocksMessage());
            socket.close();
            socket = null;
        }
    }
    
    @Override
    public void run()
    {
        if (portfolio == null) return;
        
        if (portfolio.getSymbolNames() == null)
        {
            return;
        }
        
        StreamStocksMessage startMessage = new StreamStocksMessage();
        for (String stock : portfolio.getSymbolNames())
        {
            startMessage.Add(stock);
        }
        send(startMessage);
        
        
        while (_isMonitoring)
        {
            // Try to recieve a message
            try
            {
                TickerMessage message = receive(1000);
                if (message != null)
                {
                    portfolio.updateSymbol(message);
                }
            }
            catch(Exception ex)
            {
                // TODO: Handle the error
            }
        }
    }
    
    private void send(StreamStocksMessage message)
    {
        if (message == null)
            return;
        
        byte[] bytesToSend = message.Encode();
        
        try
        {
            // Send using udp
            DatagramPacket packet = new DatagramPacket(bytesToSend, bytesToSend.length, remoteEndPoint, port);
            socket.send(packet);
        }
        catch (Exception ex)
        {
            // TODO: Handle the error.
        }
    }
    
    private TickerMessage receive(int timeout)
    {
        TickerMessage message = null;
        
        try
        {
            byte[] receivedBytes = receiveBytes(timeout);
            if (receivedBytes != null && receivedBytes.length > 0)
            {
                message = TickerMessage.Decode(receivedBytes);   
            }
        }
        catch(IOException ex)
        {
                // TODO: Handle the exception
        }
        
        return message;
    }
    
    private byte[] receiveBytes(int timeout) throws IOException
    {
        byte[] receivedBytes = null;
        byte[] buf = new byte[42];
        
        try {
            // GEt bytes via client
            socket.setSoTimeout(timeout);
            InetAddress senderEndPoint = InetAddress.getLocalHost();
        
            DatagramPacket packet = new DatagramPacket(buf, buf.length, senderEndPoint, port);
        
            socket.receive(packet);
            receivedBytes = packet.getData();
        
        } catch (IOException ex) {
            if (ex.getClass() != SocketTimeoutException.class
                    && ex.getClass() != InterruptedIOException.class)
                throw ex;
        }
         
        return receivedBytes;
    }

    @Override
    public void update() {
        this.stop();
        this.start();
    }
}
