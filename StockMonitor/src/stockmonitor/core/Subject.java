/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author ammon
 */
public class Subject {
    private Lock myLock = new ReentrantLock();
    private List<Observer> observers;
    
    public Subject()
    {
        observers = new ArrayList<>();
    }
    
    // For unit tests
    public List<Observer> getObservers()
    {
        List<Observer> temp = new ArrayList<>(observers);
        return temp;
    }
    
    public void register(Observer observer)
    {
       myLock.lock();
        try {
            {
                if (observer != null && !observers.contains(observer))
                {
                    observers.add(observer);
                }
            }
        } finally {
            myLock.unlock();
        }
    }
    
    public void unregister(Observer observer)
    {
        myLock.lock();
        try {
            {
                if (observers.contains(observer))
                {
                    observers.remove(observer);
                }
            }
        } finally {
            myLock.unlock();
        }
    }
    
    public void notifyObservers()
    {
        myLock.lock();
        try {
            for (Observer obs : observers)
            {
                if (obs != null)
                {
                    obs.update();
                }
            }
        } finally {
            myLock.unlock();
        }
    }
}
