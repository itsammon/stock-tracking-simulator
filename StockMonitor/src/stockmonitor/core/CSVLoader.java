/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class CSVLoader {
    private String csvFile;
    private ConcurrentMap<String, String> symbols;
    
    public CSVLoader()
    {
        
    }
    
    public CSVLoader(String csv)
    {
        loadFile(csv);
    }
    
    public final void loadFile(String filename)
    {
        BufferedReader reader = null;
        String line;
        String[] splitLine;
        
        // Try to load the csv file
        try {
            csvFile = filename;
            reader = new BufferedReader(new FileReader(csvFile));
            Scanner scan = new Scanner(reader);
            
            // Create the symbol table
            symbols = new ConcurrentHashMap<>();
            
            // Load all the symbols from the file
            while (scan.hasNextLine())
            {
                line = scan.nextLine();
                splitLine = line.split(",", 2);
                
                // Only add the symbol if it maps to a different company
                if (!symbols.containsValue(splitLine[1]))
                {
                    symbols.put(splitLine[0], splitLine[1]);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSVLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(CSVLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public boolean isValid(String symbol)
    {
        return symbols.containsKey(symbol);
    }
    
    public String getName(String symbol)
    {
        return symbols.get(symbol);
    }
    
    public Set<String> getSymbols()
    {
        return symbols.keySet();
    }
    
    public String getFilename()
    {
        return csvFile;
    }
}
