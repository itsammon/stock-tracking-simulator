/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import Messages.TickerMessage;

/**
 *
 * @author ammon
 */
public class Symbol extends Subject{
    private String symbolName;
    private double openingPrice = 0;
    private double prevClosingPrice = 0;
    private double currentPrice = 0;
    private double lastPrice = 0;
    private double bidPrice = 0;
    private double askPrice = 0;
    private double currentVolume = 0;
    private double averageTenDayVolume = 0;
    private long currentTimestamp = 0;
    
    public Symbol(String name)
    {
        symbolName = name;
    }
    
    public String getSymbolName()
    {
        return symbolName;
    }
    
    public void setOpeningPrice(double price)
    {
        if (price >= 0.0)
        {
            openingPrice = price;
            notifyObservers();
        }
    }
    
    public double getOpeningPrice()
    {
        return openingPrice;
    }
            
    public void setPrevClosingPrice(double price)
    {
        if (price >= 0.0)
        {
            prevClosingPrice = price;
            notifyObservers();
        }
    }
    
    public double getPrevClosingPrice()
    {
        return prevClosingPrice;
    }
    
    public void setCurrentPrice(double price)
    {
        if (price >= 0.0)
        {
            lastPrice = currentPrice;
            currentPrice = price;
            notifyObservers();
        }
    }
    
    public double getCurrentPrice()
    {
        return currentPrice;
    }
    
    public void setBidPrice(double price)
    {
        if (price >= 0.0)
        {
            bidPrice = price;
            notifyObservers();
        }
    }
    
    public double getBidPrice()
    {
        return bidPrice;
    }
    
    public void setAskPrice(double price)
    {
        if (price >= 0.0)
        {
            askPrice = price;
            notifyObservers();
        }
    }
    
    public double getAskPrice()
    {
        return askPrice;
    }
    
    public void setCurrentVolume(double volume)
    {
        if (volume >= 0.0)
        {
            currentVolume = volume;
            notifyObservers();
        }
    }
    
    public double getCurrentVolume()
    {
        return currentVolume;
    }
    
    public void setAverageTenDayVolume(double volume)
    {
        if (volume >= 0.0)
        {
            averageTenDayVolume = volume;
            notifyObservers();
        }
    }
    
    public double getAverageTenDayVolume()
    {
        return averageTenDayVolume;
    }
    
    public void setCurrentTimestamp(long timestamp)
    {
        if (timestamp >= 0)
        {
            currentTimestamp = timestamp;
            notifyObservers();
        }
    }
    
    public long getCurrentTimestamp()
    {
        return currentTimestamp;
    }
    
    public void update(TickerMessage message)
    {
        setOpeningPrice(message.getOpeningPrice());
        setPrevClosingPrice(message.getPreviousClosingPrice());
        setCurrentPrice(message.getCurrentPrice());
        setBidPrice(message.getBidPrice());
        setAskPrice(message.getAskPrice());
        setCurrentVolume(message.getCurrentVolume());
        setAverageTenDayVolume(message.getAverageVolume());
        setCurrentTimestamp(message.getMessageTimestamp());
        notifyObservers();
    }
    
    public double getLastPrice()
    {
        return lastPrice;
    }
    
    @Override
    public String toString()
    {
        return symbolName;
    }
}