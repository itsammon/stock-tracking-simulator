/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor;

import java.awt.EventQueue;
import stockmonitor.userinterface.StockMonitorFrame;

/**
 *
 * @author ammon
 */
public class StockMonitor {
    private static StockMonitorFrame main;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            
            @Override
            public void run() {
                main = new StockMonitorFrame("Stock Monitor");
        
                main.show();      
            }
        });
    }
    
}
