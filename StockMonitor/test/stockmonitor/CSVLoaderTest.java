/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor;

import stockmonitor.core.CSVLoader;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class CSVLoaderTest {
    
    public CSVLoaderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isValid method, of class CSVLoader.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        String symbol = "AAC";
        CSVLoader instance = new CSVLoader("CompanyList.csv");
        boolean expResult = true;
        boolean result = instance.isValid(symbol);
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class CSVLoader.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String symbol = "AAC";
        CSVLoader instance = new CSVLoader("CompanyList.csv");
        String expResult = "AAC Holdings, Inc.";
        String result = instance.getName(symbol);
        assertEquals(expResult, result);
    }
    
}
