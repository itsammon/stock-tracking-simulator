/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class SubjectTest {
    
    private static Observer testObserver;
    
    public SubjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        testObserver = new ObserverImpl();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of register method, of class Subject.
     */
    @Test
    public void testRegister() {
        System.out.println("register");
        Observer observer = testObserver;
        Subject instance = new Subject();
        instance.register(observer);
        assertFalse(instance.getObservers().isEmpty());
    }

    /**
     * Test of unregister method, of class Subject.
     */
    @Test
    public void testUnregister() {
        System.out.println("unregister");
        Observer observer = testObserver;
        Subject instance = new Subject();
        instance.register(observer);
        instance.unregister(observer);
        assertTrue(instance.getObservers().isEmpty());
    }

    /**
     * Test of notifyObservers method, of class Subject.
     */
    @Test
    public void testNotifyObservers() {
        System.out.println("notifyObservers");
        Observer observer = testObserver;
        Subject instance = new Subject();
        instance.register(observer);
        instance.notifyObservers();
        ObserverImpl temp = (ObserverImpl)testObserver;
        assertTrue(temp.isUpdated());
    }
    
    public static class ObserverImpl implements Observer {
        
        public boolean updated = false;
        
        public boolean isUpdated()
        {
            return updated;
        }

        @Override
        public void update() {
            updated = true;
        }
    }
    
}
