/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import Messages.TickerMessage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class PortfolioTest {
    private CSVLoader csv;
    
    public PortfolioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        csv = new CSVLoader("CompanyList.csv");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addSymbol method, of class Portfolio.
     */
    @Test
    public void testAddSymbol_Symbol() throws Exception {
        System.out.println("addSymbol");
        Symbol symbol = new Symbol("GOOG");
        Portfolio instance = new Portfolio(csv);
        instance.addSymbol(symbol);
        assertNotNull(instance.getSymbol("GOOG"));
    }

    /**
     * Test of addSymbol method, of class Portfolio.
     */
    @Test
    public void testAddSymbol_String() throws Exception {
        System.out.println("addSymbol");
        String symbol = "GOOG";
        Portfolio instance = new Portfolio(csv);
        instance.addSymbol(symbol);
        assertNotNull(instance.getSymbol("GOOG"));
    }

    /**
     * Test of addSymbols method, of class Portfolio.
     */
    @Test
    public void testAddSymbols() throws Exception {
        System.out.println("addSymbols");
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        assertNotNull(instance.getSymbol("GOOG"));
    }

    /**
     * Test of removeSymbol method, of class Portfolio.
     */
    @Test
    public void testRemoveSymbol_String() throws Exception {
        System.out.println("removeSymbol");
        String symbol = "GOOG";
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        instance.removeSymbol(symbol);
        assertNull(instance.getSymbol("GOOG"));
    }

    /**
     * Test of removeSymbol method, of class Portfolio.
     */
    @Test
    public void testRemoveSymbol_Symbol() throws Exception{
        System.out.println("removeSymbol");
        Symbol symbol = new Symbol("GOOG");
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        instance.removeSymbol(symbol);
        assertNull(instance.getSymbol("GOOG"));
    }

    /**
     * Test of removeSymbols method, of class Portfolio.
     */
    @Test
    public void testRemoveSymbols() throws Exception{
        System.out.println("removeSymbols");
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        instance.removeSymbols(symbols);
        assertNull(instance.getSymbol("GOOG"));
    }

    /**
     * Test of getSymbolNames method, of class Portfolio.
     */
    @Test
    public void testGetSymbolNames() throws Exception{
        System.out.println("getSymbolNames");
        List<String> expResult = new ArrayList<>();
        expResult.add("GOOG");
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        List<String> result = instance.getSymbolNames();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSymbols method, of class Portfolio.
     */
    @Test
    public void testGetSymbols() throws Exception{
        System.out.println("getSymbols");
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        List<Symbol> result = instance.getSymbols();
        assertEquals(symbols, result);
    }

    /**
     * Test of getSymbol method, of class Portfolio.
     */
    @Test
    public void testGetSymbol() throws Exception{
        System.out.println("getSymbol");
        String name = "GOOG";
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        instance.addSymbols(symbols);
        Symbol expResult = new Symbol("GOOG");
        Symbol result = instance.getSymbol(name);
        assertEquals(expResult.getSymbolName(), result.getSymbolName());
    }

    /**
     * Test of savePortfolio method, of class Portfolio.
     */
    @Test
    public void testSavePortfolio() throws Exception {
        System.out.println("savePortfolio");
        File file = new File("testPortfolio");
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(new Symbol("GOOG"));
        Portfolio instance = new Portfolio(csv);
        Portfolio expPort = new Portfolio(csv);
        expPort.addSymbols(symbols);
        instance.addSymbols(symbols);
        instance.savePortfolio(file);
        instance.loadPortfolio(file);
        assertEquals(expPort.getSymbolNames(), instance.getSymbolNames());
    }
    
}
