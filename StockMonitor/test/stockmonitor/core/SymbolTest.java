/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stockmonitor.core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class SymbolTest {
    
    public SymbolTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSymbolName method, of class Symbol.
     */
    @Test
    public void testGetSymbolName() {
        System.out.println("getSymbolName");
        Symbol instance = new Symbol("Test");
        String expResult = "Test";
        String result = instance.getSymbolName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOpeningPrice method, of class Symbol.
     */
    @Test
    public void testSetOpeningPrice() {
        System.out.println("setOpeningPrice");
        double price = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setOpeningPrice(price);
        
        assertEquals(price, instance.getOpeningPrice(), 0.001);
    }
    /**
     * Test of setOpeningPrice method, of class Symbol.
     */
    @Test
    public void testSetOpeningPrice2() {
        System.out.println("setOpeningPrice");
        double price = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setOpeningPrice(price);
        
        assertEquals(expResult, instance.getOpeningPrice(), 0.001);
    }
    

    /**
     * Test of setPrevClosingPrice method, of class Symbol.
     */
    @Test
    public void testSetPrevClosingPrice() {
        System.out.println("setPrevClosingPrice");
        double price = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setPrevClosingPrice(price);
        
        assertEquals(price, instance.getPrevClosingPrice(), 0.001);
    }

    /**
     * Test of setPrevClosingPrice method, of class Symbol.
     */
    @Test
    public void testSetPrevClosingPrice2() {
        System.out.println("setPrevClosingPrice");
        double price = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setPrevClosingPrice(price);
        
        assertEquals(expResult, instance.getPrevClosingPrice(), 0.001);
    }

    /**
     * Test of setCurrentPrice method, of class Symbol.
     */
    @Test
    public void testSetCurrentPrice() {
        System.out.println("setCurrentPrice");
        double price = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentPrice(price);
        
        assertEquals(price, instance.getCurrentPrice(), 0.001);
    }

    /**
     * Test of setCurrentPrice method, of class Symbol.
     */
    @Test
    public void testSetCurrentPrice2() {
        System.out.println("setCurrentPrice");
        double price = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentPrice(price);
        
        assertEquals(expResult, instance.getCurrentPrice(), 0.001);
    }

    /**
     * Test of setBidPrice method, of class Symbol.
     */
    @Test
    public void testSetBidPrice() {
        System.out.println("setBidPrice");
        double price = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setBidPrice(price);
        
        assertEquals(price, instance.getBidPrice(), 0.001);
    }

    /**
     * Test of setBidPrice method, of class Symbol.
     */
    @Test
    public void testSetBidPrice2() {
        System.out.println("setBidPrice");
        double price = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setBidPrice(price);
        
        assertEquals(expResult, instance.getBidPrice(), 0.001);
    }

    /**
     * Test of setAskPrice method, of class Symbol.
     */
    @Test
    public void testSetAskPrice() {
        System.out.println("setAskPrice");
        double price = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setAskPrice(price);
        
        assertEquals(price, instance.getAskPrice(), 0.001);
    }

    /**
     * Test of setAskPrice method, of class Symbol.
     */
    @Test
    public void testSetAskPrice2() {
        System.out.println("setAskPrice");
        double price = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setAskPrice(price);
        
        assertEquals(expResult, instance.getAskPrice(), 0.001);
    }

    /**
     * Test of setCurrentVolume method, of class Symbol.
     */
    @Test
    public void testSetCurrentVolume() {
        System.out.println("setCurrentVolume");
        double volume = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentVolume(volume);
        
        assertEquals(volume, instance.getCurrentVolume(), 0.001);
    }

    /**
     * Test of setCurrentVolume method, of class Symbol.
     */
    @Test
    public void testSetCurrentVolume2() {
        System.out.println("setCurrentVolume");
        double volume = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentVolume(volume);
        
        assertEquals(expResult, instance.getCurrentVolume(), 0.001);
    }

    /**
     * Test of setAverageTenDayVolume method, of class Symbol.
     */
    @Test
    public void testSetAverageTenDayVolume() {
        System.out.println("setAverageTenDayVolume");
        double volume = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setAverageTenDayVolume(volume);
        
        assertEquals(volume, instance.getAverageTenDayVolume(), 0.001);
    }

    /**
     * Test of setAverageTenDayVolume method, of class Symbol.
     */
    @Test
    public void testSetAverageTenDayVolume2() {
        System.out.println("setAverageTenDayVolume");
        double volume = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setAverageTenDayVolume(volume);
        
        assertEquals(expResult, instance.getAverageTenDayVolume(), 0.001);
    }

    /**
     * Test of setCurrentTimestamp method, of class Symbol.
     */
    @Test
    public void testSetCurrentTimestamp() {
        System.out.println("setCurrentTimestamp");
        long timestamp = 10;
        Symbol instance = new Symbol("Test");
        instance.setCurrentTimestamp(timestamp);
        
        assertEquals(timestamp, instance.getCurrentTimestamp());
    }

    /**
     * Test of setCurrentTimestamp method, of class Symbol.
     */
    @Test
    public void testSetCurrentTimestamp2() {
        System.out.println("setCurrentTimestamp");
        long timestamp = -1;
        long expResult = 0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentTimestamp(timestamp);
        
        assertEquals(expResult, instance.getCurrentTimestamp());
    }

    /**
     * Test of getLastPrice method, of class Symbol.
     */
    @Test
    public void testGetLastPrice() {
        System.out.println("getLastPrice");
        double price = 10.0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentPrice(price);
        instance.setCurrentPrice(price);
        
        assertEquals(price, instance.getLastPrice(), 0.0001);
    }
    
    /**
     * Test of getLastPrice method, of class Symbol.
     */
    @Test
    public void testGetLastPrice2() {
        System.out.println("getCurrentTimestamp");
        double price = -1.0;
        double expResult = 0.0;
        Symbol instance = new Symbol("Test");
        instance.setCurrentPrice(price);
        instance.setCurrentPrice(price);
        
        assertEquals(expResult, instance.getLastPrice(), 0.0001);
    }
    
}
